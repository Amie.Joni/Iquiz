/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Amine
 * Cette classe est utilisee pour se connecter a une base de donnees SQLITE
 */
public class SqliteConnection {
    
    public static Connection connector() throws ClassNotFoundException{
            Class.forName("org.sqlite.JDBC");
            Connection conn = null;
        try {
            //se connecter a la bd
            conn = DriverManager.getConnection("jdbc:sqlite:src\\db\\IquizDB.sqlite");
        } catch (SQLException ex) {
            Logger.getLogger(SqliteConnection.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Erreur de connexion a la BD");
        }
        return conn;
    }
    
}// fin sqliteconnection
