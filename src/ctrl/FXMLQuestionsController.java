/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import Model.DocumentModel;
import Model.QuestionsModel;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Amine
 */
public class FXMLQuestionsController implements Initializable {

    //le modele qui contient les fonctions utilitaires
    public QuestionsModel questionsmodel = new QuestionsModel();

    //naviguer entre les questions
    private int indexQuestion = 0;

    //liste des questions , choix des reponses
    ArrayList<Integer> questions;
    ArrayList<String> suggestions;
    @FXML
    private Label labelEnoncer;
     @FXML
    private RadioButton b1;
     @FXML
    private Button questsuiv;
    @FXML
    private ToggleGroup group;
    @FXML
    private Label numQ;
    @FXML
    private RadioButton b2;

    @FXML
    private RadioButton b3;
    int numberNumQ=1;
    int numberCorrect=0;
    String reponse;
    @FXML
    private RadioButton b4;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            if (questionsmodel.isConnected()) {

                questions = questionsmodel.getQuestions(questionsmodel.getCurrentCategorieId());
                int a = questions.size()-1;
                numQ.setText(1+"/"+ a);
                System.out.println(questionsmodel.getCurrentJoueurId()); // l'utilisateur courant
                System.out.println(questionsmodel.getCurrentCategorieId()); // la categorie courante
                System.out.println(questions.size());   //la taille de la liste des questions
                
                //afficher la question courante
                labelEnoncer.setText(questionsmodel.getQuestionById(questions.get(indexQuestion)));
                ArrayList<String> suggestion = questionsmodel.getSuggestionsByID(questions.get(indexQuestion));
                Collections.shuffle(suggestion);
                    
                 b1.setText(suggestion.get(0));
                 b2.setText(suggestion.get(1));
                 b3.setText(suggestion.get(2));
                 b4.setText(suggestion.get(3));
            } else {
                System.out.println("Non connecter a la BD");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLQuestionsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //passer a la question suivante
    public void nextQuestion(ActionEvent event) throws SQLException, IOException {
        //incremente indexQuestion pour passer a la question suivante
        if (indexQuestion < questions.size()-2) {
            
            if (b1.isSelected()){
                reponse = b1.getText();
                b1.setSelected(false);}
            else if (b2.isSelected()){
                reponse = b2.getText();
                b2.setSelected(false);}
            else if (b3.isSelected()){
                reponse = b3.getText();
                b3.setSelected(false);}
            else {
                reponse = b4.getText();
                b4.setSelected(false);}
            
            if (reponse.equals(questionsmodel.getCorrectSuggestionsByID(questions.get(indexQuestion))) ){
                
                Integer MonScore = questionsmodel.getScore(questionsmodel.getCurrentPartieId()) +1;
                questionsmodel.UpdatePartieScore(questionsmodel.getCurrentPartieId(),MonScore);
            }
                indexQuestion++;
            
            labelEnoncer.setText(questionsmodel.getQuestionById(questions.get(indexQuestion)));
                ArrayList<String> suggestion = questionsmodel.getSuggestionsByID(questions.get(indexQuestion));
                Collections.shuffle(suggestion);
                b1.setText(suggestion.get(0));
                b2.setText(suggestion.get(1));
                //b3.setText(suggestion.get(2));
                //b4.setText(suggestion.get(3));
                numberNumQ++;
                int num = questions.size()-1;
                numQ.setText(numberNumQ+"/"+ num);
                
                
        }
        else{
            ((Node) event.getSource()).getScene().getWindow().hide();
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/quiz/FXMLResult.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
                    
        }
            
    }
    
        public void PrintScore(ActionEvent event) throws IOException {
        ((Node) event.getSource()).getScene().getWindow().hide();
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/quiz/FXMLResult.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    

}
