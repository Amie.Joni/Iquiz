/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import Model.DocumentModel;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Amine
 */
public class FXMLDocumentController implements Initializable {

    //le modele qui contient les fonctions utilitaires
    public DocumentModel documentmodel = new DocumentModel();

    //les composants du fxml
    @FXML
    private TextField pseudoField;
    @FXML
    private PasswordField codeField;
    @FXML
    private Label scoreField;
    @FXML
    private ChoiceBox categorieChoice;
    @FXML
    private Label errLabel;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        errLabel.setText("");
        //si on est connecte avec la base de donnees
        if (documentmodel.isConnected()) {
            //mettre a jour la liste des scores
            try {
                //rappatrier les donnees et les adaptees puis les affichees
                ArrayList<String> arr = documentmodel.getScore();
                String str = "";
                str = arr.stream().map((element) -> element + '\n').reduce(str, String::concat);
                scoreField.setText(str);
            } catch (SQLException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
            //mettre a jour la liste des categories
            try {
                //rappatrier les donnees et les adaptees puis les affichees
                ArrayList<String> catarr = documentmodel.getCategorie();
                catarr.forEach((element) -> {
                    categorieChoice.getItems().add(element);
                });
            } catch (SQLException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            errLabel.setText("Non connecter a la BD");
        }

    }

    //apres clique sur commencer nouvelle partie
    public void login(ActionEvent event) {
        if (!"".equals(pseudoField.getText()) && !"".equals(codeField.getText()) && null != categorieChoice.getValue()) {
            try {
                //si le joueur exist deja
                if (documentmodel.joueurExist(pseudoField.getText())) {
                    if (documentmodel.joueurConnexion(pseudoField.getText(), codeField.getText())) {
                        ((Node) event.getSource()).getScene().getWindow().hide(); //cacher la fenetre courante
                        //affichage de la fenetre des question
                        Stage stage = new Stage();
                        FXMLLoader loader = new FXMLLoader();
                        loader.setLocation(getClass().getResource("/quiz/FXMLQuestions.fxml"));
                        Parent root = loader.load();
                        Scene scene = new Scene(root);
                        stage.setScene(scene);
                        stage.show();
                        //creer une partie
                        if(!documentmodel.commencerPatie(documentmodel.getCategorieId((String) categorieChoice.getValue()),documentmodel.getJoueurId(pseudoField.getText()),0)) {
                            System.out.println("Erreur lors de la creation d'une partie");
                        }
                    } else {
                        errLabel.setText("Code incorrect !");
                    }
                    //si le joueur n'existe pas
                } else {
                    //enregistrer le joueur
                    if (documentmodel.enregitrerJoueur(pseudoField.getText(), codeField.getText())) {
                        //affichage de la fenetre des question
                        Stage stage = new Stage();
                        FXMLLoader loader = new FXMLLoader();
                        loader.setLocation(getClass().getResource("/quiz/FXMLQuestions.fxml"));
                        Parent root = loader.load();
                        Scene scene = new Scene(root);
                        stage.setScene(scene);
                        stage.show();
                        //creer une partie
                        if(!documentmodel.commencerPatie(documentmodel.getCategorieId((String) categorieChoice.getValue()),documentmodel.getJoueurId(pseudoField.getText()),0)) {
                            System.out.println("Erreur lors de la creation d'une partie");
                        }
                    } else {
                        errLabel.setText("Erreur lors de l'enregistrement");
                    }
                }
            } catch (SQLException | IOException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);

            }
        }else{
            errLabel.setText("Renseigner les tous les champs ! (categorie,pseudo,code)");
        }
    }

    public void closeWindow(ActionEvent event) {
        System.exit(0);
    }

}
