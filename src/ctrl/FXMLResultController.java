/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;


import Model.ResultModel;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Amine
 */
public class FXMLResultController implements Initializable {
    
    //le modele qui contient les fonctions utilitaires
    public ResultModel resultmodel = new ResultModel();
    
    @FXML
    private Label scoreLabel;
    @FXML
    private Label nicknameLabel;
    @FXML
    private Label catLabel;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        try {
            if (resultmodel.isConnected()) {
                /*
                System.out.println(resultmodel.getCurrentCategorieName());
                System.out.println(resultmodel.getCurrentJoueurName());
                System.out.println(resultmodel.getCurrentScore());
                System.out.println(resultmodel.getNumberOfQuestions(resultmodel.getCurrentCategorieName()));
                */
                int a = resultmodel.getNumberOfQuestions(resultmodel.getCurrentCategorieName())-1;
                scoreLabel.setText(String.valueOf(resultmodel.getCurrentScore())+"/"+a);
                nicknameLabel.setText(resultmodel.getCurrentJoueurName());
                catLabel.setText(resultmodel.getCurrentCategorieName());
            } else {
                System.out.println("Non connecter a la BD");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLQuestionsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void PrintScore(ActionEvent event) throws IOException {
        ((Node) event.getSource()).getScene().getWindow().hide();
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/quiz/FXMLDocument.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    public void closeWindow(ActionEvent event) {
        System.exit(0);
    }
}
