/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import quiz.SqliteConnection;

/**
 *
 * @author Amine
 */
public class QuestionsModel {

    Connection connection;//objet utilise pour se connecter a la BD

    public QuestionsModel() { //on etablit la connexion dans le constructeur
        try {
            connection = SqliteConnection.connector();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Connexion impossible avec la BD depuis DocumentModel");
        }
        //fermer l'app
        if (connection == null) {
            System.exit(1);
        }
    }

    //verification de la connexion a la bd
    public boolean isConnected() {
        try {
            return !connection.isClosed();
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    //retourner la liste des id des questions/categorie choisi
    public ArrayList<Integer> getQuestions(Integer idcat) throws SQLException {
        ArrayList<Integer> listqu = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        String query = "select id from question where id_categorie = ?";
        
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, idcat);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                listqu.add(Integer.parseInt(result.getString("id")));
            }
            return listqu;
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);

        } finally {
            preparedStatement.close();
            result.close();
        }
        return null;
    }
    
    
    
    
    
    public String getQuestionById(int index) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        String query = "select enoncer from question where id = ?";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, index);
            result = preparedStatement.executeQuery();
            if (result.next()) {
                return result.getString("enoncer");
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            preparedStatement.close();
            result.close();
        }
        return null;
    }
    
    public ArrayList<String> getSuggestionsByID (int index) throws SQLException{
        ArrayList<String> listSug = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        String query = "select enoncer from reponse where id_question = ?";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, index);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                listSug.add(result.getString("enoncer"));
            }
            return listSug;
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            preparedStatement.close();
            result.close();
        }
        return null;
    }  
    public String getCorrectSuggestionsByID (int index) throws SQLException{
        
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        String query = "select enoncer from reponse where id_question = ? and correct = 1 ";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, index);
            result = preparedStatement.executeQuery();
            if (result.next()) {
                return result.getString("enoncer");
            }
            return null;
            
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            preparedStatement.close();
            result.close();
        }
        return null;
    }
    
        //retourner id de la categorie courante
    public Integer getCurrentCategorieId() throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        String query = "select id_categorie from partie order by id desc limit 1";
        try {
            preparedStatement = connection.prepareStatement(query);
            result = preparedStatement.executeQuery();
            if (result.next()) {
                return Integer.parseInt(result.getString("id_categorie"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            preparedStatement.close();
            result.close();
        }
        return null;
    }
    //retourner id de la categorie courante
    public Integer getCurrentJoueurId() throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        String query = "select id_joueur from partie order by id desc limit 1";
        try {
            preparedStatement = connection.prepareStatement(query);
            result = preparedStatement.executeQuery();
            if (result.next()) {
                return Integer.parseInt(result.getString("id_joueur"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            preparedStatement.close();
            result.close();
        }
        return null;
    }
    //mettre a jour le score d'une partie
    public Boolean UpdatePartieScore(Integer partieid, Integer score) throws SQLException {
        PreparedStatement preparedStatement = null;
        String query = "update partie set score=? where id=?";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, score);
            preparedStatement.setInt(2, partieid);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            preparedStatement.close();
        }
        return null;
    }
    //retourner id de la partie courante voila l id de la  mettre a jour le score 
    public Integer getCurrentPartieId() throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        String query = "select id from partie order by id desc limit 1";
        try {
            preparedStatement = connection.prepareStatement(query);
            result = preparedStatement.executeQuery();
            if (result.next()) {
                
                return result.getInt("id");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            preparedStatement.close();
            result.close();
        }
        return null;
    }
    public Integer getScore (int id) throws SQLException{
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        String query = "select score from partie where id=?";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            result = preparedStatement.executeQuery();
            if (result.next()) {
                return result.getInt("score");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            preparedStatement.close();
            result.close();
        }
        return null;
    }
}
