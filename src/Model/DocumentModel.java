/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import quiz.SqliteConnection;

/**
 *
 * @author Amine
 */
public class DocumentModel {

    Connection connection;//objet utilise pour se connecter a la BD

    public DocumentModel() { //on etablit la connexion dans le constructeur
        try {
            connection = SqliteConnection.connector();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Connexion impossible avec la BD depuis DocumentModel");
        }
        //fermer l'app
        if (connection == null) {
            System.exit(1);
        }
    }

    //verification de la connexion a la bd
    public boolean isConnected() {
        try {
            return !connection.isClosed();
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    //connecter le joueur
    public boolean joueurConnexion(String nickname, String code) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        String query = "select * from joueur where pseudo=? and code=?";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, nickname);
            preparedStatement.setString(2, code);
            result = preparedStatement.executeQuery();
            return result.next();
        } catch (SQLException e) {
            
        } finally {
            preparedStatement.close();
            result.close();
        }
        return false;
    }

    //retourner la liste des scores sous format de chaine -> cat1->user(20)
    public ArrayList<String> getScore() throws SQLException {
        ArrayList<String> listsc = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            String query = "select score,nom,pseudo from partie \n"
                    + "inner join categorie on  categorie.id = partie.id_categorie \n"
                    + "inner join joueur on  joueur.id = partie.id_joueur \n"
                    + "order by score DESC limit 15";
            preparedStatement = connection.prepareStatement(query);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                //cat1->user(20)
                listsc.add(result.getString("nom") +"->"+ result.getString("pseudo") + "(" + result.getString("score") + ")");
            }
            return listsc;
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            preparedStatement.close();
            result.close();
        }
        return null;
    }
    //retourner la liste des categories
    public ArrayList<String> getCategorie() throws SQLException {
        ArrayList<String> listcat = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            String query = "select nom from categorie ";

            preparedStatement = connection.prepareStatement(query);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                listcat.add(result.getString("nom"));
            }
            return listcat;
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            preparedStatement.close();
            result.close();
        }
        return null;
    }
    //enregistrer un nouveau joueur
    public boolean enregitrerJoueur(String ps, String cd) throws SQLException {
        PreparedStatement preparedStatement = null;
        String query = "insert into joueur(pseudo,code) values(?,?) ";

        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, ps);
            preparedStatement.setString(2, cd);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);

        } finally {
            preparedStatement.close();
        }
        return false;
    }
    //verifier est ce qu'un joueur exist deja
    public boolean joueurExist(String nickname) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        String query = "select * from joueur where pseudo=?";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, nickname);
            result = preparedStatement.executeQuery();
            return result.next();
        } catch (SQLException e) {
        } finally {
            preparedStatement.close();
            result.close();
        }
        return false;
    }
    //ajouter une nouvelle partie
    public boolean commencerPatie(Integer catid,Integer jouid,Integer score) throws SQLException {
        PreparedStatement preparedStatement = null;
        String query = "insert into partie(id_categorie,id_joueur,score) values(?,?,?) ";
        try {
            System.out.println("-"+catid+"-"+jouid+"-"+score);
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, catid);
            preparedStatement.setInt(2, jouid);
            preparedStatement.setInt(3, score);
            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            preparedStatement.close();
        }
        return false;
    }
    //retourner id categorie
    public Integer getCategorieId(String n) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        String query = "select id from categorie where nom =?";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, n);
            result = preparedStatement.executeQuery();
            if (result.next()) {
                return Integer.parseInt(result.getString("id"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            preparedStatement.close();
            result.close();
        }
        return null;
    }
    //retourner id user
    public  Integer getJoueurId(String n) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        String query = "select id from joueur where pseudo =?";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, n);
            result = preparedStatement.executeQuery();
            if (result.next()) {
                return Integer.parseInt(result.getString("id"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            preparedStatement.close();
            result.close();
        }
        return null;
    }
    
}//fin Documentmodele
