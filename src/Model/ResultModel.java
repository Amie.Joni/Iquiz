/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import quiz.SqliteConnection;

/**
 *
 * @author Amine
 */
public class ResultModel {
    
    Connection connection;//objet utilise pour se connecter a la BD

    public ResultModel() { //on etablit la connexion dans le constructeur
        try {
            connection = SqliteConnection.connector();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ResultModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Connexion impossible avec la BD depuis DocumentModel");
        }
        //fermer l'app
        if (connection == null) {
            System.exit(1);
        }
    }
    
    //verification de la connexion a la bd
    public boolean isConnected() {
        try {
            return !connection.isClosed();
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    //retourner pseudo du joueur courante
    public String getCurrentJoueurName() throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        String query =  "select joueur.pseudo from partie \n" +
                        "inner join joueur on id_joueur = joueur.id\n" +
                        "order by partie.id desc limit 1";
        try {
            preparedStatement = connection.prepareStatement(query);
            result = preparedStatement.executeQuery();
            if (result.next()) {
                return result.getString("pseudo");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            preparedStatement.close();
            result.close();
        }
        return null;
    }
    //retourner nom de la categorie courante
    public String getCurrentCategorieName() throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        String query =  "select categorie.nom from partie \n" +
                        "inner join categorie on id_categorie = categorie.id\n" +
                        "order by partie.id desc limit 1";
        try {
            preparedStatement = connection.prepareStatement(query);
            result = preparedStatement.executeQuery();
            if (result.next()) {
                return result.getString("nom");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            preparedStatement.close();
            result.close();
        }
        return null;
    }
    //retourner le score de la partie courante
    public Integer getCurrentScore() throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        String query = "select score from partie order by partie.id desc limit 1";
        try {
            preparedStatement = connection.prepareStatement(query);
            result = preparedStatement.executeQuery();
            if (result.next()) {
                return Integer.parseInt(result.getString("score"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            preparedStatement.close();
            result.close();
        }
        return null;
    }
    //retourner le nombre de question pour une categorie
    public Integer getNumberOfQuestions(String Cat) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        String query =  "select count(*) as nbr  from question \n" +
                        "inner join categorie on categorie.id = id_categorie\n" +
                        "where nom =?";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, Cat);
            result = preparedStatement.executeQuery();
            if (result.next()) {
                return result.getInt("nbr");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DocumentModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            preparedStatement.close();
            result.close();
        }
        return null;
    }
    
}
